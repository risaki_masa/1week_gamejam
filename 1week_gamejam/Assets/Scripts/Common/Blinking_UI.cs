﻿using UnityEngine;
using System.Collections;

public class Blinking_UI : MonoBehaviour
{
    [SerializeField]
    private float m_interval = 0.7f;

    [SerializeField]
    Renderer m_renderer;

    private IEnumerator Blink() 
    {
        while ( true ) 
        {
            yield return new WaitForSeconds( m_interval );
            m_renderer.enabled = !m_renderer.enabled;
        }
    }

    private void OnEnable()
    {
        m_renderer.enabled = true;
        StartCoroutine( Blink() );
    }
}
