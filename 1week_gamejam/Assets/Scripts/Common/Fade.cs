﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    [SerializeField]
    private Image m_image;

    [SerializeField]
    private float m_time = 0.5f;

    private void Start()
    {
        StartCoroutine( Fade_In() );
    }

    private IEnumerator Fade_In()
    {
        var elapsed_time = 0.0f;

        while ( elapsed_time < m_time ) 
        {
            var ratio = Get_Ratio( elapsed_time );
            var alpha = 1.0f - ratio;

            m_image.color = new Color( 0.0f, 0.0f, 0.0f, alpha );

            yield return null;

            elapsed_time += Time.deltaTime;
        }

        m_image.enabled = false;
    }

    public void Fade_Out( Action on_completed ) 
    {
        StartCoroutine( Do_Fade_Out( on_completed ) );
    }

    private IEnumerator Do_Fade_Out( Action on_completed ) 
    {
        m_image.enabled = true;

        var elapsed_time = 0.0f;

        while ( elapsed_time < m_time )
        {
            var ratio       = Get_Ratio( elapsed_time );
            m_image.color   = new Color( 0.0f, 0.0f, 0.0f, ratio );

            yield return null;

            elapsed_time += Time.deltaTime;
        }

        on_completed();
    }

    private float Get_Ratio( float elapsed_time ) 
    {
        var ratio = elapsed_time / m_time;
        if ( ratio > 1.0f ) ratio = 1.0f;

        return ratio;
    }
}
