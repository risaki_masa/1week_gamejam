﻿using UnityEngine;

static class Input_Manager
{
    public static bool Is_Pushed() 
    {
        return Input.GetMouseButtonDown( 0 );
    }

    public static bool Is_Released() 
    {
        return Input.GetMouseButtonUp( 0 );
    }

    public static Vector3 Get_Input_Pos()
    {
        var pos = Input.mousePosition;
        return new Vector3( pos.x, pos.y, pos.z );
    }
}

