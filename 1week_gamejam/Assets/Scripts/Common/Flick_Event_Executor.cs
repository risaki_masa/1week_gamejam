﻿using UnityEngine;
using System;

using FlickEvent        = System.Action<UnityEngine.Vector2>;
using FlickEventList    = System.Collections.Generic.List<System.Action<UnityEngine.Vector2>>;

public class Flick_Event_Executor : MonoBehaviour
{
    private FlickEventList m_on_flicking_list_called_always = new FlickEventList();
    private FlickEventList m_on_flicked_list_called_always  = new FlickEventList();
    private FlickEventList m_on_flicking_list               = new FlickEventList();
    private FlickEventList m_on_flicked_list                = new FlickEventList();

    private Vector3 m_pushed_pos;
    private bool    m_is_pushing;

    private void Update()
    {
        if ( Input_Manager.Is_Pushed() )    On_Pushed();
        if ( m_is_pushing )                 On_Pushing();
        if ( Input_Manager.Is_Released() )  On_Released();
    }

    private void On_Pushed() 
    {
        m_pushed_pos = Input_Manager.Get_Input_Pos();
        m_is_pushing = true;
    }

    private void On_Pushing()
    {
        var distance = Get_Distance_From_Pushed_To_Curent();
        Action<FlickEvent> on_flicking = element => element( distance );

        m_on_flicking_list.ForEach( on_flicking );
        m_on_flicking_list_called_always.ForEach( on_flicking );
    }

    private void On_Released() 
    {
        var distance = Get_Distance_From_Pushed_To_Curent();
        Action<FlickEvent> on_flicked = element => element( distance );

        m_on_flicked_list.ForEach( on_flicked );
        m_on_flicked_list_called_always.ForEach( on_flicked );

        m_on_flicking_list.Clear();
        m_on_flicked_list.Clear();

        m_is_pushing = false;
    }

    private Vector2 Get_Distance_From_Pushed_To_Curent() 
    {
        var current_pos = Input_Manager.Get_Input_Pos();

        return new Vector2( current_pos.x - m_pushed_pos.x, current_pos.y - m_pushed_pos.y );
    }

    public void Register_On_Flicking( FlickEvent on_flicking )
    {
        m_on_flicking_list.Add( on_flicking );
    }

    public void Register_On_Flicked( FlickEvent on_flicked ) 
    {
        m_on_flicked_list.Add( on_flicked );
    }

    public void Register_On_Flicking_Called_Always( FlickEvent on_flicking ) 
    {
        m_on_flicking_list_called_always.Add( on_flicking );   
    }

    public void Register_On_Flicked_Called_Always( FlickEvent on_flicked )
    {
        m_on_flicked_list_called_always.Add( on_flicked );
    }
}
