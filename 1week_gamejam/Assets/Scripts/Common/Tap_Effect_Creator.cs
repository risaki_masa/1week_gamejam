﻿using UnityEngine;

public class Tap_Effect_Creator : MonoBehaviour
{
    [SerializeField]
    private GameObject m_effect_prefab;

    [SerializeField]
    private Transform m_canvas_transform;

    private void Update()
    {
        if ( !Input_Manager.Is_Pushed() ) return;

        var pos     = Input_Manager.Get_Input_Pos();
        var effect  = Instantiate( m_effect_prefab, pos, Quaternion.identity );

        effect.transform.SetParent( m_canvas_transform );
    }
}
