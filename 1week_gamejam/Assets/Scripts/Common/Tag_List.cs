﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class Tag_List
{
    private const string m_base_name = "Base";
    private const string m_player_chara_name = "Player_Chara";
    private const string m_enemy_chara_name = "Enemy_Chara";

    static public string Base { get { return m_base_name; } }
    static public string Player_Chara { get { return m_player_chara_name; } }
    static public string Enemy_Chara { get { return m_enemy_chara_name; } }
}
