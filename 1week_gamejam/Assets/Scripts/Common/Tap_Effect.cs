﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tap_Effect : MonoBehaviour
{
    [SerializeField]
    private float m_time = 0.3f;

    [SerializeField]
    private float m_max_scale = 12.0f;

    [SerializeField]
    private RectTransform m_transform;

    [SerializeField]
    private Image m_image;

    private void Start()
    {
        StartCoroutine( Animate() );
    }

    private IEnumerator Animate()
    {
        var elapsed_time    = 0.0f;
        var to_scale        = new Vector3( m_max_scale, m_max_scale, 1.0f );

        do
        {
            elapsed_time += Time.deltaTime;
            var ratio = elapsed_time / m_time;
            if ( ratio >= 1.0f ) ratio = 1.0f;

            m_transform.localScale  = Vector3.Lerp( Vector3.one, to_scale, ratio );

            var color       = m_image.color;
            color.a         = 1.0f - Mathf.Pow( ratio, 2 );
            m_image.color   = color;

            yield return null;

        } while ( elapsed_time < m_time );

        Destroy( gameObject );
    }
}
