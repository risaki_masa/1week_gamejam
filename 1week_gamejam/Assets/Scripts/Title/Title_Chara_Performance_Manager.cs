﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title_Chara_Performance_Manager : MonoBehaviour
{
    public enum State
    {
        Parade, 
        Look_In, 

        Num_States
    }

    [SerializeField] private float m_wait_time = 0.0f;

    [SerializeField] private Vector3 m_parade_start_offset = new Vector3(0.0f, 0.0f, 0.0f);
    [SerializeField] private Vector3 m_parade_neighbor_interval = new Vector3(250.0f, 0.0f, 0.0f);
    [SerializeField] private float m_parade_ran_distance_magnification = 1.0f;

    [SerializeField] private Vector3 m_look_in_start_offset = new Vector3(0.0f, -400.0f, 0.0f);
    [SerializeField] private float m_look_in_size_magnification = 1.5f;
    [SerializeField] private float m_look_in_face_up_rate = 0.5f;

    [SerializeField] private List<Title_Performance_Chara> m_player_chara_list = new List<Title_Performance_Chara>();
    //[SerializeField] private List<Title_Performance_Chara> m_enemy_chara_list = new List<Title_Performance_Chara>();

    private State m_state;

    [SerializeField] private RectTransform m_canvas_transform;

    private float m_wait_timer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        Update_Timer();   
    }

    private void Initialize()
    {
        m_canvas_transform = GameObject.Find("Canvas").GetComponent<RectTransform>();

        m_state = State.Parade;

        End_Performance();
    }

    private void Update_Timer()
    {
        if(m_wait_timer > 0.0f)
        {
            m_wait_timer -= Time.deltaTime;

            if(m_wait_timer <= 0.0f)
            {
                Decide_State();
            }
        }
    }

    public void End_Performance()
    {
        for (int i = 0; i < m_player_chara_list.Count; ++i)
        {
            m_player_chara_list[i].gameObject.SetActive(false);
        }

        //for (int i = 0; i < m_enemy_chara_list.Count; ++i)
        //{
        //    m_enemy_chara_list[i].gameObject.SetActive(false);
        //}

        m_wait_timer = m_wait_time;
    }

    private void Decide_State()
    {
        switch(m_state)
        {
            case State.Parade:
                Setup_Parade_State();

                break;
            case State.Look_In:
                Setup_Look_In_State();

                break;
        }

        m_state = (State)((int)m_state + 1);
        if (m_state == State.Num_States) m_state = (State)0;

    }

    private void Setup_Parade_State()
    {
        m_state = State.Parade;

        float canvas_scale = m_canvas_transform.localScale.x;

        Vector3 start_pos = (new Vector3(Screen.width * 0.5f, -Screen.height * 0.5f, 0.0f) / canvas_scale + m_parade_start_offset * m_parade_ran_distance_magnification);
        for(int i = 0; i < m_player_chara_list.Count; ++i)
        {
            Title_Performance_Chara current_chara = m_player_chara_list[i];
            current_chara.gameObject.SetActive(true);

            Vector3 current_start_pos = start_pos;

            float ran_distance = (Screen.width / canvas_scale + m_parade_start_offset.x + m_parade_neighbor_interval.x * m_player_chara_list.Count * m_parade_ran_distance_magnification);

            current_chara.Setup_Parade_State(i, current_start_pos, ran_distance, m_parade_neighbor_interval.x);
        }
    }

    private void Setup_Look_In_State()
    {
        m_state = State.Look_In;

        int look_in_chara_index = Random.Range(0, m_player_chara_list.Count);

        Title_Performance_Chara look_in_chara = m_player_chara_list[look_in_chara_index];
        look_in_chara.gameObject.SetActive(true);

        float canvas_scale = m_canvas_transform.localScale.x;

        Vector3 start_pos = (new Vector3(0.0f, -Screen.height * 0.5f, 0.0f) / canvas_scale + m_look_in_start_offset);
        float up_height = -m_look_in_start_offset.y * m_look_in_face_up_rate;

        look_in_chara.Setup_Look_In_State(start_pos, m_look_in_size_magnification, up_height);
    }
}
