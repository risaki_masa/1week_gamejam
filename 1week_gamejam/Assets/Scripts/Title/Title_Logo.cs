﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title_Logo : MonoBehaviour
{
    [SerializeField] float m_start_spd = 0.0f;
    [SerializeField] float m_decelerate_spd = 0.0f;

    private RectTransform m_transform;

    private float m_spd;

    // Start is called before the first frame update
    void Start()
    {
        m_transform = GetComponent<RectTransform>();

        m_spd = m_start_spd;
    }

    // Update is called once per frame
    void Update()
    {
        float prev_rot_z = m_transform.rotation.eulerAngles.z;

        m_transform.Rotate(new Vector3(0.0f, 0.0f, m_spd * Time.deltaTime));

        float current_rot_z = m_transform.rotation.eulerAngles.z;

        if (Mathf.Abs(current_rot_z - prev_rot_z) > 300.0f) m_decelerate_spd *= -1;
        m_spd -= m_decelerate_spd * Time.deltaTime;
    }
}
