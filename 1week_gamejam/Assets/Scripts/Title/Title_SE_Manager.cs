﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title_SE_Manager : MonoBehaviour
{
    public enum Se_Kind
    {
        Game_Start,
        Explanation_Open, 
        Explanation_Close,
    }

    [SerializeField] private List<AudioClip> m_se_list;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public AudioClip Get_SE(Se_Kind kind) { return m_se_list[(int)kind]; }
}
