﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Title_Stage_Select_Button : MonoBehaviour
{
    [SerializeField]
    private List<Button> m_buttons = new List<Button>();

    [SerializeField]
    private AudioSource m_se_player;

    [SerializeField]
    private Title_SE_Manager m_se_manager;

    [SerializeField]
    private Fade m_fade;

    private AudioClip m_se;

    private void Start()
    {
        // TODO:    一先ず「ゲーム開始」時のSEと同じものを用いる
        //          後により良いSEがあった場合は差し替えること
        m_se = m_se_manager.Get_SE( Title_SE_Manager.Se_Kind.Game_Start );
    }

    public void On_Clicked_Stage1() 
    {
        On_Clicked( "Stage1" );
    }

    public void On_Clicked_Stage2()
    {
        On_Clicked( "Stage2" );
    }

    public void On_Clicked_Stage3() 
    { 
        On_Clicked( "Stage3" );
    }

    private void On_Clicked( string loading_scene_name )
    {
        m_se_player.PlayOneShot( m_se );
        m_buttons.ForEach( button => button.interactable = false );

        m_fade.Fade_Out( () => SceneManager.LoadScene( loading_scene_name ) );
    }
}
