﻿using UnityEngine;

public class Title_Game_Start : MonoBehaviour
{
    [SerializeField]
    private GameObject m_stage_select_button;

    [SerializeField]
    private AudioSource m_se_player;

    private AudioClip m_start_se;

    // Start is called before the first frame update
    void Start()
    {
        Title_SE_Manager se_manager = GameObject.Find("SE_Manager").GetComponent<Title_SE_Manager>();
        m_start_se = se_manager.Get_SE(Title_SE_Manager.Se_Kind.Game_Start);
    }

    public void Click_Button()
    {
        m_se_player.PlayOneShot(m_start_se);

        gameObject.SetActive( false );
        m_stage_select_button.SetActive( true );
    }
}
