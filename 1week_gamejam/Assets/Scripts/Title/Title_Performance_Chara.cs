﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title_Performance_Chara : MonoBehaviour
{
    static private Vector3 s_parade_start_vel = new Vector3(-200.0f, 50.0f, 0.0f);
    static private Vector3 s_parade_add_vel = new Vector3(0.0f, -300.0f, 0.0f);

    static private Vector3 s_look_in_vel = new Vector3(0.0f, 100.0f, 0.0f);
    static private float s_look_in_wait_time = 1.0f;

    private Title_Chara_Performance_Manager m_manager;

    private Title_Chara_Performance_Manager.State m_state;

    private bool m_is_leader;
    private Vector3 m_vel = Vector3.zero;

    private Vector3 m_start_pos;

    private float m_parade_ran_distance;
    private float m_parade_delay_timer;

    private float m_look_in_up_height;
    private float m_look_in_wait_timer;

    // Start is called before the first frame update
    void Start()
    {
        m_manager = GameObject.Find("Chara_Performance_Manager").GetComponent<Title_Chara_Performance_Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(m_state)
        {
            case Title_Chara_Performance_Manager.State.Parade:
                Update_Parade_State();

                break;
            case Title_Chara_Performance_Manager.State.Look_In:
                Update_Look_In_State();

                break;
        }
    }

    public void Setup_Parade_State(int index, Vector3 start_pos, float ran_distance, float neighbor_interval_x)
    {
        m_state = Title_Chara_Performance_Manager.State.Parade;

        m_is_leader = index == 0;

        m_start_pos = start_pos;
        m_parade_ran_distance = ran_distance;
        m_parade_delay_timer = (neighbor_interval_x * Time.deltaTime) / (-s_parade_start_vel.x * Time.deltaTime) * index;

        RectTransform transform = GetComponent<RectTransform>();
        transform.anchoredPosition3D = m_start_pos;

        m_vel = s_parade_start_vel;
    }

    public void Setup_Look_In_State(Vector3 start_pos, float size_magnification, float up_height)
    {
        m_state = Title_Chara_Performance_Manager.State.Look_In;

        m_start_pos = start_pos;
        m_look_in_up_height = up_height;

        RectTransform transform = GetComponent<RectTransform>();
        transform.localScale = new Vector3(size_magnification, size_magnification);
        transform.anchoredPosition3D = m_start_pos;

        m_look_in_wait_timer = 0.0f;

        m_vel = s_look_in_vel;
    }

    public void Update_Parade_State()
    {
        if (m_parade_delay_timer > 0.0f)
        {
            m_parade_delay_timer -= Time.deltaTime;

            if (m_parade_delay_timer <= 0.0f)
            {
                m_parade_delay_timer = 0.0f;
            }
        }
        else
        {
            RectTransform transform = GetComponent<RectTransform>();
            transform.anchoredPosition3D += m_vel * Time.deltaTime;

            m_vel += s_parade_add_vel * Time.deltaTime;


            if (transform.anchoredPosition3D.y <= m_start_pos.y)
            {
                m_vel = s_parade_start_vel;
            }

            if (transform.anchoredPosition3D.x <= m_start_pos.x - m_parade_ran_distance && m_is_leader)
            {
                m_manager.End_Performance();
            }
        }
    }

    public void Update_Look_In_State()
    {
        if (m_look_in_wait_timer > 0.0f)
        {
            m_look_in_wait_timer -= Time.deltaTime;

            if(m_look_in_wait_timer <= 0.0f)
            {
                m_look_in_wait_timer = 0.0f;
                m_vel.y *= -1.0f;
            }
        }
        else if (m_vel.y > 0.0f)
        {
            RectTransform transform = GetComponent<RectTransform>();
            transform.anchoredPosition3D += m_vel * Time.deltaTime;

            if(transform.anchoredPosition3D.y >= m_start_pos.y + m_look_in_up_height)
            {
                m_look_in_wait_timer = s_look_in_wait_time;
            }
        }
        else
        {
            RectTransform transform = GetComponent<RectTransform>();
            transform.anchoredPosition3D += m_vel * Time.deltaTime;

            if(transform.anchoredPosition3D.y <= m_start_pos.y)
            {
                transform.localScale = Vector3.one;

                m_manager.End_Performance();
            }
        }
    }
}
