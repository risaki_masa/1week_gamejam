﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title_Explanation_Button : MonoBehaviour
{
    [SerializeField] private AudioSource m_se_player;

    [SerializeField] private string m_explanation_str = "説明";
    [SerializeField] private string m_back_str = "戻る";

    [SerializeField] private GameObject m_explanation_obj = null;
    private Text m_text;

    private AudioClip m_open_se;
    private AudioClip m_close_se;

    private bool m_is_open = false;

    // Start is called before the first frame update
    void Start()
    {
        m_explanation_obj.SetActive(false);
        m_text = transform.Find("Text").GetComponent<Text>();
        m_text.text = m_explanation_str;

        Title_SE_Manager se_manager = GameObject.Find("SE_Manager").GetComponent<Title_SE_Manager>();
        m_open_se = se_manager.Get_SE(Title_SE_Manager.Se_Kind.Explanation_Open);
        m_close_se = se_manager.Get_SE(Title_SE_Manager.Se_Kind.Explanation_Close);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Click()
    {
        if (m_is_open) Close_Explanation();
        else Open_Explanation();
    }

    public void Open_Explanation()
    {
        m_is_open = true;

        m_explanation_obj.SetActive(true);
        m_text.text = m_back_str;
        m_se_player.PlayOneShot(m_open_se);
    }

    public void Close_Explanation()
    {
        m_is_open = false;

        m_explanation_obj.SetActive(false);
        m_text.text = m_explanation_str;
        m_se_player.PlayOneShot(m_close_se);
    }
}
