﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Wave_Effect : MonoBehaviour
{
    private SpriteRenderer m_renderer;

    [SerializeField] private float m_spd;
    [SerializeField] private float m_min_alpha;

    private Vector3 m_default_scale;

    private float m_max_scale_length;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Spread();
    }

    public void Initialize(float radius)
    {
        m_renderer = GetComponent<SpriteRenderer>();

        m_default_scale = transform.localScale;

        m_max_scale_length = radius * m_default_scale.x;

        transform.localScale = Vector3.zero;
    }

    public void Spread()
    {
        Vector3 scale = transform.localScale;

        if(scale.x >= m_max_scale_length)
        {
            Destroy(gameObject);
        }

        scale.x += m_spd * Time.deltaTime;
        scale.y += m_spd * Time.deltaTime;
        transform.localScale = scale;

        Color color = m_renderer.color;
        color.a = transform.localScale.x / m_max_scale_length * (1.0f - m_min_alpha) + m_min_alpha;
        m_renderer.color = color;
    }
}
