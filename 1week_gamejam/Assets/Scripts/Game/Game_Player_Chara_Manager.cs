﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Player_Chara_Manager : MonoBehaviour
{
    [SerializeField] private Transform m_base_transform;
    [SerializeField] private Vector3 m_lineup_offset;
    [SerializeField] private Vector3 m_neighbor_offset;

    [SerializeField] private List<Game_Player_Chara> m_player_chara_list;

    private Vector3 m_lineup_pos;

    private List<Game_Player_Chara> m_in_base_chara_list = new List<Game_Player_Chara>();
    private List<int> m_in_base_chara_pos_index_list = new List<int>();

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Initialize()
    {
        m_lineup_pos = m_base_transform.position + m_lineup_offset;

        for(int i = 0; i < m_player_chara_list.Count; ++i)
        {
            m_in_base_chara_list.Add(m_player_chara_list[i]);
            m_in_base_chara_pos_index_list.Add(i);
        }

        Relocate_Player_Chara();
    }

    public void Back_Base_Player_Chara(Game_Player_Chara player_chara, out Vector3 locate_pos)
    {
        m_in_base_chara_list.Add(player_chara);

        //  左から来たら左端から、右から来たら右端から並ばせる
        bool is_left;
        Vector3 lineup_pos_to_player_chara_vec = player_chara.transform.position - m_lineup_pos;
        if (lineup_pos_to_player_chara_vec.x <= 0.0f) is_left = true;
        else is_left = false;

        int player_chara_pos_index = 0;
        for(int i = 0; i < m_in_base_chara_pos_index_list.Count; ++i)
        {
            int current_pos_index = m_in_base_chara_pos_index_list[i];

            if (current_pos_index < player_chara_pos_index) continue;

            if (is_left)
            {
                if (current_pos_index == 0)
                {
                    player_chara_pos_index = current_pos_index + 1;
                }
                else if(current_pos_index % 2 == 1)
                {
                    player_chara_pos_index = current_pos_index + 2;
                }
            }
            else
            {
                if(current_pos_index % 2 == 0)
                {
                    player_chara_pos_index = current_pos_index + 2;
                }
            }
        }

        m_in_base_chara_pos_index_list.Add(player_chara_pos_index);

        locate_pos = Calc_Pos_By_Index(player_chara_pos_index);
    }

    public void Out_Base_Player_Chara(Game_Player_Chara player_chara)
    {
        if (!m_in_base_chara_list.Contains(player_chara)) return;

        int in_base_player_chara_index;
        in_base_player_chara_index = m_in_base_chara_list.IndexOf(player_chara);

        m_in_base_chara_list.RemoveAt(in_base_player_chara_index);
        m_in_base_chara_pos_index_list.RemoveAt(in_base_player_chara_index);

        Relocate_Player_Chara();
    }

    private void Relocate_Player_Chara()
    {
        int center_pos_num = 0;
        int left_max_index = 0;
        int right_max_index = 0;
        for (int i = 0; i < m_in_base_chara_pos_index_list.Count; ++i)
        {
            int current_index = m_in_base_chara_pos_index_list[i];

            if (current_index == 0) continue;

            if (current_index % 2 == 1)
            {
                --center_pos_num;

                if (current_index > left_max_index) left_max_index = current_index;
            }
            else
            {
                ++center_pos_num;

                if (current_index > right_max_index) right_max_index = current_index;
            }
        }

        if (center_pos_num > 0) --center_pos_num;
        else if (center_pos_num < 0) ++center_pos_num;

        List<int> space_num_list = new List<int>();
        for (int add_num = 1; add_num <= left_max_index; add_num += 2)
        {
            space_num_list.Add(add_num);
        }
        for(int add_num = 0; add_num <= right_max_index; add_num += 2)
        {
            space_num_list.Add(add_num);
        }
        for(int i = 0; i < m_in_base_chara_pos_index_list.Count; ++i)
        {
            space_num_list.Remove(m_in_base_chara_pos_index_list[i]);
        }

        for(int i = 0; i < m_in_base_chara_pos_index_list.Count; ++i)
        {
            int relocate_index = m_in_base_chara_pos_index_list[i];

            int shorten_num = 0;
            for(int j = 0; j < space_num_list.Count; ++j)
            {
                int current_space_index = space_num_list[j];
                if(relocate_index % 2 == current_space_index % 2 && current_space_index < relocate_index)
                {
                    shorten_num += 2;
                }
            }

            relocate_index -= shorten_num;

            if (center_pos_num < 0)
            {
                if (relocate_index % 2 == 0)
                {
                    relocate_index -= center_pos_num * 2;
                }
                else
                {
                    relocate_index += center_pos_num * 2;
                    if (relocate_index < 0)
                    {
                        relocate_index = relocate_index * -1 - 1;
                    }
                }
            }
            else
            {
                if (relocate_index % 2 == 0)
                {
                    relocate_index -= center_pos_num * 2;
                    if(relocate_index < 0)
                    {
                        relocate_index = relocate_index * -1 - 1;
                    }
                }
                else
                {
                    relocate_index += center_pos_num * 2;
                }
            }

            m_in_base_chara_pos_index_list[i] = relocate_index;
        }

        for(int i = 0; i < m_in_base_chara_list.Count; ++i)
        {
            m_in_base_chara_list[i].Relocate_In_Base_Wait_Pos(Calc_Pos_By_Index(m_in_base_chara_pos_index_list[i]));
        }
    }

    private Vector3 Calc_Pos_By_Index(int pos_index)
    {
        bool is_left;
        if (pos_index % 2 == 1) is_left = true;
        else is_left = false;

        int what_number_from_center = (pos_index + 1) / 2;

        Vector3 offset = m_neighbor_offset;
        if (is_left) offset.x *= -1;

        Vector3 pos = m_lineup_pos;
        pos += offset * what_number_from_center;

        return pos;
    }

    public void Dead_Chara(Game_Chara chara)
    {
        for(int i = 0; i < m_player_chara_list.Count; ++i)
        {
            m_player_chara_list[i].Dead_Other_Chara(chara);
        }
    }
}
