﻿using UnityEngine;
using System.Linq;
using System.Collections;

public class Game_Chara_Selector : MonoBehaviour
{
    [SerializeField]
    private Game_Chara_Shooter m_shooter;

    [SerializeField]
    private GameObject m_ui;

    [SerializeField]
    private Vector3 m_ui_offset;

    private bool m_is_selecting;
    private bool m_is_showing_ui;

    private void Update()
    {
        if ( m_is_selecting || !Input_Manager.Is_Released() ) return;

        var ray             = Camera.main.ScreenPointToRay( Input.mousePosition );
        var target_sprite   = Physics.RaycastAll( ray )
            .Select         ( hit_data      => hit_data.collider.gameObject )
            .FirstOrDefault ( game_object   => 
            {
                var is_player_chara     = game_object.tag == Tag_List.Player_Chara;
                var has_billboard       = game_object.GetComponent<Billboard>() != null;
                var is_target_sprite    = is_player_chara && has_billboard;

                return is_target_sprite;
            } );

        if ( target_sprite == null ) return;

        var chara = target_sprite.GetComponentInParent<Game_Player_Chara>();
        if ( chara == null || !chara.In_Base ) return;

        m_is_selecting = true;

        StartCoroutine( Show_And_Update_UI( chara ) );
        m_shooter.Shoot(
            chara,
            () => m_is_showing_ui   = false,
            () => m_is_selecting    = false 
        );
    }

    private IEnumerator Show_And_Update_UI( Game_Player_Chara chara )
    {
        m_ui.SetActive( true );
        m_is_showing_ui = true;

        while ( m_is_showing_ui ) 
        {
            m_ui.transform.localPosition = chara.transform.localPosition + m_ui_offset;
            yield return null;
        }

        m_ui.SetActive( false );
    }
}
