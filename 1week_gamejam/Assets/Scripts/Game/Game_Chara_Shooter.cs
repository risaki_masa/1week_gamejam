﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class Game_Chara_Shooter : MonoBehaviour
{
    [SerializeField]
    private Flick_Event_Executor m_event_executor;

    [SerializeField]
    private SpriteRenderer m_shooting_vector_renderer;

    [SerializeField]
    private Game_Camera_Controller m_camera_controller;

    [SerializeField]
    private Vector3 m_shoot_point = new Vector3( 0, 0, -40.0f );

    [SerializeField]
    private GameObject m_tap_point;

    [SerializeField]
    private Vector2 m_max_range = new Vector2( 30.0f, 130.0f );

    [SerializeField]
    private float m_invalid_range = 10.0f;

    [SerializeField]
    private float m_gravity = 60.0f;

    [SerializeField]
    private float m_speed_per_second = 30.0f;

    [SerializeField]
    private GameObject m_range_sprite;

    private float m_angle_of_degree;
    private float m_magnitude;

    public void Shoot( Game_Player_Chara chara, Action on_shooted, Action on_completed ) 
    {
        m_camera_controller.Move_To_Away( () => 
        {
            Wait_Until_Pushed_Start_Point( () =>
            {
                m_range_sprite.SetActive( true );

                Register_Flick_Events( chara, on_shooted, on_completed );
            } );
        } );
    }

    private void Wait_Until_Pushed_Start_Point( Action on_completed ) 
    {
        StartCoroutine( Do_Wait_Until_Pushed_Start_Point( on_completed ) );
    }

    private IEnumerator Do_Wait_Until_Pushed_Start_Point( Action on_completed )
    {
        var is_pushed = false;

        do {
            if ( !Input_Manager.Is_Pushed() )
            {
                yield return null;
                continue;
            }

            var ray     = Camera.main.ScreenPointToRay( Input.mousePosition );
            is_pushed   = Physics.RaycastAll( ray )
                .Select ( hit_data  => hit_data.collider.gameObject.name )
                .Any    ( name      => name == m_tap_point.name )
            ;

            yield return null;
        } while ( !is_pushed );

        on_completed();
    }

    private void Register_Flick_Events( Game_Player_Chara chara, Action on_shooted, Action on_completed ) 
    {
        m_event_executor.Register_On_Flicking( On_Flicking );
        m_event_executor.Register_On_Flicked( distance =>
        {
            On_Flicked( distance, chara, on_shooted, on_completed );
        } );
    }

    private void On_Flicking( Vector2 distance )
    {
        var angle_of_degree         = Calculate_Angle_Of_Degree( distance );
        var clamped_distance        = Clamp_Distance( distance, angle_of_degree );
        var clamped_angle_of_degree = Calculate_Angle_Of_Degree( clamped_distance );
        var vector_transform        = m_shooting_vector_renderer.transform;

        vector_transform.localRotation      = Quaternion.Euler( 0.0f, 0.0f, clamped_angle_of_degree );
        m_shooting_vector_renderer.size     = new Vector2( 1, clamped_distance.magnitude );

        m_angle_of_degree   = clamped_angle_of_degree;
        m_magnitude         = clamped_distance.magnitude;
    }

    private float Calculate_Angle_Of_Degree( Vector2 distance ) 
    {
        var angle   = Mathf.Atan2( distance.y, distance.x ) * Mathf.Rad2Deg;
        angle       += 180.0f;

        if ( angle > 180.0f && angle <= 270.0f ) angle = 180.0f;
        if ( angle > 270.0f && angle <= 360.0f ) angle = 0.0f;

        return angle;
    }

    private Vector2 Clamp_Distance( Vector2 distance, float angle_of_degree ) 
    {
        var angle_of_radian = angle_of_degree * Mathf.Deg2Rad;
        var range_x         = Mathf.Abs( m_max_range.x * Mathf.Cos( angle_of_radian ) );
        var range_y         = Mathf.Abs( m_max_range.y * Mathf.Sin( angle_of_radian ) );

        var x = Mathf.Clamp( distance.x, -range_x, range_x );
        var y = Mathf.Clamp( distance.y, -range_y, range_y );

        return new Vector2( x, y );
    }

    private void On_Flicked( Vector2 distance, Game_Player_Chara chara, Action on_shooted, Action on_completed ) 
    {
        m_shooting_vector_renderer.size = Vector2.zero;

        if ( m_magnitude < m_invalid_range ) 
        {
            Wait_Until_Pushed_Start_Point( () =>
            {
                Register_Flick_Events( chara, on_shooted, on_completed );
            } );

            return;
        }

        m_range_sprite.SetActive( false );

        on_shooted();

        Move_Chara( chara );
        m_camera_controller.Move_To_Closer( on_completed );
    }

    private void Move_Chara( Game_Player_Chara chara ) 
    {
        StartCoroutine( Do_Move_Chara( chara ) );
    }

    private IEnumerator Do_Move_Chara( Game_Player_Chara chara )
    {
        chara.Thrown();

        var chara_transform = chara.transform;
        var required_time   = m_magnitude / m_speed_per_second;
        var elapsed_time    = 0.0f;
        var landing_point   = Get_Landing_Point();

        do
        {
            elapsed_time += Time.deltaTime;
            chara_transform.localPosition = Calculate_Current_Pos( elapsed_time, required_time, landing_point );

            yield return null;

        } while ( elapsed_time < required_time );

        chara.To_Land();
    }

    private Vector3 Get_Landing_Point() 
    {
        var rotation    = Quaternion.Euler( 0.0f, -m_angle_of_degree, 0.0f );
        var direction   = rotation * Vector3.right;
        var distance    = direction * m_magnitude;
        
        return m_shoot_point + distance;
    }

    private Vector3 Calculate_Current_Pos( float elapsed_time, float required_time, Vector3 landing_point )
    {
        var ratio = elapsed_time / required_time;
        if ( ratio >= 1.0f ) ratio = 1.0f;

        var pos = Vector3.Lerp( m_shoot_point, landing_point, ratio );
        pos.y   = Calclate_Pos_Y_By_Vertial_Throw( elapsed_time, required_time );

        return pos;
    }

    private float Calclate_Pos_Y_By_Vertial_Throw( float elapsed_time, float required_time )
    {
        var initial_velocity = m_gravity * required_time * 0.5f;

        if ( elapsed_time > required_time ) elapsed_time = required_time;

        var movement_by_velocity    = initial_velocity * elapsed_time;
        var movement_by_gravity     = -0.5f * m_gravity * elapsed_time * elapsed_time;
        var pos                     = movement_by_velocity + movement_by_gravity;

        return pos;
    }
}
