﻿using System;
using System.Collections;
using UnityEngine;

public class Game_Camera_Controller : MonoBehaviour
{
    [SerializeField]
    private float m_moving_time = 0.5f;

    [SerializeField]
    private Vector3 m_closer_pos;

    [SerializeField]
    private Vector3 m_closer_angles;

    [SerializeField]
    private Vector3 m_away_pos;

    [SerializeField]
    private Vector3 m_away_angles;

    [SerializeField]
    private Flick_Event_Executor m_event_executor;

    [SerializeField]
    [Range( 0.01f, 1.0f )]
    private float m_flick_speed = 0.1f;

    [SerializeField]
    private Vector3 m_max_pos = new Vector3( 60.0f, 0.0f, 300.0f );

    [SerializeField]
    private Vector3 m_min_pos = new Vector3( -60.0f, 0.0f, -80.0f );

    private Transform   m_transform;
    private Vector2     m_previous_distance;
    private bool        m_can_move_by_flick = true;

    private void Start()
    {
        m_transform = Camera.main.transform;
        m_event_executor.Register_On_Flicking_Called_Always( Move_By_Flick );
        m_event_executor.Register_On_Flicked_Called_Always( distance => Reset_Previous_Distance() );
    }

    private void Move_By_Flick( Vector2 distance ) 
    {
        if ( !m_can_move_by_flick ) return;

        var distance_from_previous  = distance - m_previous_distance;
        m_previous_distance         = distance;

        var movement = distance_from_previous * -m_flick_speed;
        m_transform.localPosition = Calculate_Current_Pos( m_transform.localPosition, movement );
    }

    private Vector3 Calculate_Current_Pos( Vector3 previous_pos, Vector2 movement )
    {
        var pos_x = Mathf.Clamp( previous_pos.x + movement.x, m_min_pos.x, m_max_pos.x );
        var pos_z = Mathf.Clamp( previous_pos.z + movement.y, m_min_pos.z, m_max_pos.z );

        return new Vector3( pos_x, previous_pos.y, pos_z );
    }

    private void Reset_Previous_Distance() 
    {
        m_previous_distance = new Vector2();
    }

    public void Move_To_Closer( Action on_completed )
    {
        StartCoroutine( Do_Move_To_Closer( on_completed ) );
    }

    private IEnumerator Do_Move_To_Closer( Action on_completed )
    {
        yield return Move(
            m_away_pos,
            m_away_angles,
            m_closer_pos,
            m_closer_angles
        );

        m_can_move_by_flick = true;

        on_completed();
    }

    public void Move_To_Away( Action on_completed ) 
    {
        StartCoroutine( Do_Move_To_Away( on_completed ) );
    }

    private IEnumerator Do_Move_To_Away( Action on_completed ) 
    {
        yield return Move(
            m_transform.localPosition,
            m_transform.localEulerAngles,
            m_away_pos,
            m_away_angles
        );

        m_can_move_by_flick = false;

        on_completed();
    }

    private IEnumerator Move(
        Vector3 from_pos,
        Vector3 from_angles,
        Vector3 to_pos,
        Vector3 to_angles
    ) 
    {
        var elapsed_time = 0.0f;

        do
        {
            elapsed_time    += Time.deltaTime;
            var ratio       = elapsed_time / m_moving_time;
            if ( ratio >= 1.0f ) ratio = 1.0f;

            SetPosByLerp( from_pos, to_pos, ratio );
            SetAnglesByLerp( from_angles, to_angles, ratio );

            yield return null;

        } while ( elapsed_time < m_moving_time );
    }

    private void SetPosByLerp( Vector3 from, Vector3 to, float ratio ) 
    {
        m_transform.localPosition = Vector3.Lerp( from, to, ratio );
    }

    private void SetAnglesByLerp( Vector3 from, Vector3 to, float ratio ) 
    {
        var angles                  = Vector3.Lerp( from, to, ratio );
        m_transform.localRotation   = Quaternion.Euler( angles );
    }
}