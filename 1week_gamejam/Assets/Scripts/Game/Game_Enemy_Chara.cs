﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Enemy_Chara : Game_Chara
{
    enum State
    {
        Move_To_Base,
        Attack
    }

    private Game_Enemy_Chara_Manager m_enemy_chara_manager;
    private Game_Player_Chara_Manager m_player_chara_manager;

    private State m_state;

    // Start is called before the first frame update
    void Start()
    {
        Chara_Start();

        m_enemy_chara_manager = GameObject.Find("Enemy_Chara_Manager").GetComponent<Game_Enemy_Chara_Manager>();
        m_player_chara_manager = GameObject.Find("Player_Chara_Manager").GetComponent<Game_Player_Chara_Manager>();

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        Chara_Update();
    }

    private void Initialize()
    {
        Chara_Initialize();

        m_state = State.Move_To_Base;
    }

    protected override void Move()
    {
        switch (m_state)
        {
            case State.Move_To_Base:
                Move_To_Base();

                break;
            case State.Attack:
                Move_To_Enemy();

                break;
        }
    }

    protected override void Dead()
    {
        m_enemy_chara_manager.Dead_Enemy_Chara(this);

        m_enemy_chara_manager.Dead_Chara(this);
        m_player_chara_manager.Dead_Chara(this);

        gameObject.SetActive(false);
    }

    public override void Reach_Base()
    {
        m_base.Damage();

        m_enemy_chara_manager.Dead_Enemy_Chara(this);

        gameObject.SetActive(false);
    }

    protected override void Enter_In_Search()
    {
        if (m_state == State.Move_To_Base)
        {
            m_state = State.Attack;
        }
    }

    protected override void Empty_In_Search()
    {
        if (m_state == State.Attack)
        {
            m_state = State.Move_To_Base;
        }
    }

    public override void Dead_Other_Chara(Game_Chara chara)
    {
        Out_Search_Chara(chara);
    }
}
