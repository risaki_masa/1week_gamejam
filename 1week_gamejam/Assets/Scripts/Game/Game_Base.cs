﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Base : MonoBehaviour
{
    private Game_Result m_result;

    private AudioSource m_audio_source;
    private AudioClip m_damage_se;

    [SerializeField] private int m_life;
    [SerializeField] private GameObject m_draw_start_pos_obj;
    [SerializeField] private GameObject m_life_ui_obj_source;
    [SerializeField] private Vector3 m_life_ui_interval;

    private List<GameObject> m_life_ui_obj_list = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        m_result = GameObject.Find("Result").GetComponent<Game_Result>();
        m_audio_source = GetComponent<AudioSource>();
        Game_SE_Manager se_manager = GameObject.Find("SE_Manager").GetComponent<Game_SE_Manager>();
        m_damage_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Base_Damage);

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Initialize()
    {
        for(int i = 0; i < m_life; ++i)
        {
            GameObject current_life_ui_obj = Instantiate(m_life_ui_obj_source, m_draw_start_pos_obj.transform);
            RectTransform current_life_ui_transform = current_life_ui_obj.GetComponent<RectTransform>();
            current_life_ui_transform.localPosition = Vector3.zero;
            current_life_ui_transform.localPosition += m_life_ui_interval * i;

            m_life_ui_obj_list.Add(current_life_ui_obj);
        }
    }

    public void Damage()
    {
        if (m_life <= 0) return;

        m_life_ui_obj_list[m_life - 1].SetActive(false);

        --m_life;

        if(m_life <= 0)
        {
            m_result.Lose();
        }

        m_audio_source.PlayOneShot(m_damage_se);
    }
}
