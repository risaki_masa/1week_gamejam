﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_HP_Current_Gauge : MonoBehaviour
{
    private Game_Chara m_chara;

    private Vector3 m_default_scale;

    // Start is called before the first frame update
    void Start()
    {
        m_chara = transform.parent.parent.GetComponent<Game_Chara>();

        m_default_scale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        Update_Hp_Gauge();
    }

    private void Update_Hp_Gauge()
    {
        float hp_rate = (float)m_chara.HP / m_chara.Max_HP;

        Vector3 scale = m_default_scale;
        scale.x *= hp_rate;
        transform.localScale = scale;
    }
}
