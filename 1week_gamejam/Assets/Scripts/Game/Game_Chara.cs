﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Game_Chara : MonoBehaviour
{
    static private float s_enhance_time = 15.0f;
    static private float s_limit_under_pos = -100.0f;

    protected Game_Base m_base;

    protected AudioSource m_audio_source;
    protected AudioClip m_damage_se;

    private Game_Damage_Effect m_damage_effect;

    private bool m_is_player;
    [SerializeField] protected float m_spd;
    [SerializeField] private int m_max_hp;
    [SerializeField] protected int m_default_atk;
    [SerializeField] private float m_atk_interval;
    [SerializeField] private float m_attack_range;

    protected float m_spd_rate;
    private int m_hp;
    protected int m_atk;
    private float m_enhance_timer;
    private float m_atk_interval_timer;

    protected List<Game_Chara> m_in_search_chara_list = new List<Game_Chara>();
    private Game_Chara m_near_in_search_chara = null;
    private float m_near_in_search_chara_distance;

    private Game_Effect_Area m_on_effect_area;
    private float m_effect_timer;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    protected void Chara_Start()
    {
        Game_SE_Manager se_manager = GameObject.Find("SE_Manager").GetComponent<Game_SE_Manager>();
        m_base = GameObject.Find("Base").GetComponent<Game_Base>();

        m_audio_source = GetComponent<AudioSource>();
        m_damage_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Chara_Damage);

        m_damage_effect = transform.Find("Damage_Effect").GetComponent<Game_Damage_Effect>();
        m_damage_effect.Initialize();

        m_is_player = tag == Tag_List.Player_Chara ? true : false;
    }

    protected void Chara_Update()
    {
        Update_Timer();
        Update_Near_In_Search_Chara();
        Move();
        Check_Chara_Dead();
    }

    protected void Chara_Initialize()
    {
        m_spd_rate = 1.0f;
        m_hp = m_max_hp;
        m_atk = m_default_atk;
        m_enhance_timer = 0.0f;
        m_atk_interval_timer = 0.0f;
        m_on_effect_area = null;
        m_effect_timer = 0.0f;
    }

    protected void Move_Pos(Vector3 vel)
    {
        vel *= m_spd * m_spd_rate * Time.deltaTime;

        transform.position += vel;

    }

    private void Update_Timer()
    {
        if (m_atk_interval_timer > 0.0f) m_atk_interval_timer -= Time.deltaTime;

        if(m_enhance_timer > 0.0f)
        {
            m_enhance_timer -= Time.deltaTime;
            if(m_enhance_timer <= 0.0f)
            {
                m_enhance_timer = 0.0f;
                m_atk = m_default_atk;
            }
        }

        if(m_on_effect_area != null && m_effect_timer > 0.0f)
        {
            m_effect_timer -= Time.deltaTime;

            if(m_effect_timer <= 0.0f)
            {
                Affect_By_Area();
            }
        }
    }

    private void Update_Near_In_Search_Chara()
    {
        if (m_in_search_chara_list.Count > 0)
        {
            m_near_in_search_chara = m_in_search_chara_list[0];
            m_near_in_search_chara_distance = Vector3.Distance(transform.position, m_near_in_search_chara.transform.position);

            for(int i = 1; i < m_in_search_chara_list.Count; ++i)
            {
                Game_Chara current_chara = m_in_search_chara_list[i];
                float current_distance = Vector3.Distance(transform.position, current_chara.transform.position);
                
                if(current_distance < m_near_in_search_chara_distance)
                {
                    m_near_in_search_chara = current_chara;
                    m_near_in_search_chara_distance = current_distance;
                }
            }

        }
        else
        {
            m_near_in_search_chara = null;
            m_near_in_search_chara_distance = 0.0f;
        }
    }

    abstract protected void Move();

    private void Check_Chara_Dead()
    {
        if(transform.position.y < s_limit_under_pos)
        {
            Dead();
        }
    }

    protected void Move_To_Base()
    {
        Vector3 move_vel = m_base.transform.position - transform.position;
        move_vel.y = 0;
        move_vel.Normalize();
        Move_Pos(move_vel);
    }

    protected void Move_To_Enemy()
    {
        if (m_near_in_search_chara == null) return;

        if (m_near_in_search_chara_distance <= m_attack_range)
        {
            Attack();
        }
        else
        {
            Vector3 move_vel = m_near_in_search_chara.transform.position - transform.position;
            move_vel.y = 0;
            move_vel.Normalize();
            Move_Pos(move_vel);
        }
    }

    protected void Attack()
    {
        if (m_atk_interval_timer <= 0.0f)
        {
            bool target_is_dead;
            m_near_in_search_chara.Damage(m_atk);

            m_atk_interval_timer = m_atk_interval;
        }
    }

    public void Damage(int damage_val)
    {
        if (m_hp <= 0)
        {
            return;
        }

        if (damage_val < 0) damage_val = 0;

        m_hp -= damage_val;

        if(m_hp <= 0)
        {
            m_hp = 0;
            Dead();
        }

        m_audio_source.PlayOneShot(m_damage_se);
        m_damage_effect.Fire();
    }

    public void Heal(int heal_val)
    {
        if (m_hp <= 0) return;

        if (heal_val < 0) heal_val = 0;

        m_hp += heal_val;

        if(m_hp > m_max_hp)
        {
            m_hp = m_max_hp;
        }
    }

    public void Enhance(float enhance_coefficient)
    {
        if (m_hp <= 0) return;

        m_enhance_timer = s_enhance_time;
        m_atk = (int)(m_default_atk * enhance_coefficient);
    }

    public void Affect_By_Area()
    {
        switch(m_on_effect_area.Kind)
        {
            case Game_Effect_Area.Effect_Kind.Dot:
                Damage((int)m_on_effect_area.Effect_Val);

                break;
        }

        m_effect_timer = m_on_effect_area.Effect_Per_Time;
    }

    abstract protected void Dead();

    abstract public void Reach_Base();

    public void In_Search_Chara(Game_Chara chara)
    {
        if (m_in_search_chara_list.Count <= 0) Enter_In_Search();

        m_in_search_chara_list.Add(chara);
    }

    public void Out_Search_Chara(Game_Chara chara)
    {
        m_in_search_chara_list.Remove(chara);

        if (m_in_search_chara_list.Count <= 0) Empty_In_Search();
    }

    private void On_Effect_Area(Game_Effect_Area effect_area)
    {
        m_on_effect_area = effect_area;
        m_effect_timer = m_on_effect_area.Effect_Per_Time;

        switch(m_on_effect_area.Kind)
        {
            case Game_Effect_Area.Effect_Kind.Change_Spd:
                m_spd_rate = m_on_effect_area.Effect_Val;

                break;
        }
    }

    private void Out_Effect_Area(Game_Effect_Area effect_area)
    {
        if (m_on_effect_area != effect_area) return;

        switch (m_on_effect_area.Kind)
        {
            case Game_Effect_Area.Effect_Kind.Change_Spd:
                m_spd_rate = 1.0f;

                break;
        }

        m_on_effect_area = null;
    }

    abstract protected void Enter_In_Search();
    abstract protected void Empty_In_Search();

    abstract public void Dead_Other_Chara(Game_Chara chara);

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == Tag_List.Base)
        {
            Reach_Base();
        }

        Game_Effect_Area effect_area = other.GetComponent<Game_Effect_Area>();
        if(effect_area != null)
        {
            On_Effect_Area(effect_area);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Game_Effect_Area effect_area = other.GetComponent<Game_Effect_Area>();
        if (effect_area != null)
        {
            Out_Effect_Area(effect_area);
        }
    }

    public bool Is_Player { get { return m_is_player; } }
    public int Max_HP { get { return m_max_hp; } }
    public int HP { get { return m_hp; } }
}
