﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_SE_Manager : MonoBehaviour
{
    public enum SE_Kind
    {
        Chara_Damage, 
        Base_Damage, 
        Throw, 
        Landing_Attack, 
        Landing_Heal, 
        Landing_Enhance, 
        Win, 
        Lose, 
        Camera_Back,
    }

    [SerializeField] private List<AudioClip> m_se_list;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public AudioClip Get_SE(SE_Kind kind) { return m_se_list[(int)kind]; }
}
