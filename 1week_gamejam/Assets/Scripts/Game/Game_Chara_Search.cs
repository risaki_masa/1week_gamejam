﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Chara_Search : MonoBehaviour
{
    private Game_Chara m_chara;
    private bool m_is_player;

    // Start is called before the first frame update
    void Start()
    {
        m_chara = transform.parent.GetComponent<Game_Chara>();
        m_is_player = m_chara.Is_Player;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        Game_Chara enter_chara = other.GetComponent<Game_Chara>();
        if (enter_chara != null)
        {
            m_chara.In_Search_Chara(enter_chara);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Game_Chara exit_chara = other.GetComponent<Game_Chara>();
        if (exit_chara != null)
        {
            m_chara.Out_Search_Chara(exit_chara);
        }
    }
}
