﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Enemy_Chara_Manager : MonoBehaviour
{
    private Game_Result m_result;

    [SerializeField] private List<Game_Enemy_Chara> m_enemy_chara_list = new List<Game_Enemy_Chara>();

    private List<Game_Enemy_Chara> m_alive_enemy_chara_list = new List<Game_Enemy_Chara>();

    // Start is called before the first frame update
    void Start()
    {
        m_result = GameObject.Find("Result").GetComponent<Game_Result>();

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Initialize()
    {
        m_enemy_chara_list.Clear();
        for (int i = 0; i < transform.childCount; ++i)
        {
            Game_Enemy_Chara current_enemy_chara = transform.GetChild(i).GetComponent<Game_Enemy_Chara>();
            if (current_enemy_chara != null) m_enemy_chara_list.Add(current_enemy_chara);
        }

        m_alive_enemy_chara_list.AddRange(m_enemy_chara_list);
    }

    public void Dead_Enemy_Chara(Game_Enemy_Chara enemy_chara)
    {
        m_alive_enemy_chara_list.Remove(enemy_chara);

        if(m_alive_enemy_chara_list.Count <= 0)
        {
            m_result.Win();
        }
    }

    public void Dead_Chara(Game_Chara chara)
    {
        for (int i = 0; i < m_enemy_chara_list.Count; ++i)
        {
            m_enemy_chara_list[i].Dead_Other_Chara(chara);
        }
    }
}
