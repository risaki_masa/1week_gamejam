﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Player_Chara : Game_Chara
{
    enum State
    {
        In_Base, 
        Thrown, 
        Move_To_Base, 
        Attack
    }

    enum Landing_Skill_Kind
    {
        Attack, 
        Heal, 
        Enhance
    }

    private Game_Player_Chara_Manager m_player_chara_manager;
    private Game_Enemy_Chara_Manager m_enemy_chara_manager;

    private AudioClip m_thrown_se;
    private AudioClip m_landing_attack_se;
    private AudioClip m_landing_heal_se;
    private AudioClip m_landing_enhance_se;

    [SerializeField] Landing_Skill_Kind m_landing_skill_kind;
    [SerializeField] float m_landing_skill_arg = 1.0f;
    [SerializeField] GameObject m_landing_effect_obj;

    private State m_state;

    private bool m_in_base;

    private List<Game_Chara> m_in_landing_skill_chara_list = new List<Game_Chara>();

    private Vector3 m_in_base_wait_pos;

    // Start is called before the first frame update
    void Start()
    {
        Chara_Start();

        m_player_chara_manager = GameObject.Find("Player_Chara_Manager").GetComponent<Game_Player_Chara_Manager>();
        m_enemy_chara_manager = GameObject.Find("Enemy_Chara_Manager").GetComponent<Game_Enemy_Chara_Manager>();

        Game_SE_Manager se_manager = GameObject.Find("SE_Manager").GetComponent<Game_SE_Manager>();
        m_thrown_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Throw);
        m_landing_attack_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Landing_Attack);
        m_landing_heal_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Landing_Heal);
        m_landing_enhance_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Landing_Enhance);

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        Chara_Update();
    }

    private void Initialize()
    {
        Chara_Initialize();

        m_state = State.In_Base;
        m_in_base = true;
    }

    protected override void Move()
    {
        switch(m_state)
        {
            case State.In_Base:
                Wait_In_Base();

                break;
            case State.Thrown:

                break;
            case State.Move_To_Base:
                Move_To_Base();

                break;
            case State.Attack:
                Move_To_Enemy();

                break;
        }
    }

    protected override void Dead()
    {
        m_player_chara_manager.Dead_Chara(this);
        m_enemy_chara_manager.Dead_Chara(this);

        gameObject.SetActive(false);
    }

    public void Relocate_In_Base_Wait_Pos(Vector3 pos)
    {
        m_in_base_wait_pos = pos;
    }

    private void Wait_In_Base()
    {
        Vector3 this_pos_to_wait_pos_vec = m_in_base_wait_pos - transform.position;

        if(this_pos_to_wait_pos_vec.magnitude <= m_spd * m_spd_rate * Time.deltaTime)
        {
            transform.position = m_in_base_wait_pos;
        }
        else
        {
            Vector3 move_vel = this_pos_to_wait_pos_vec;
            move_vel.y = 0;
            move_vel.Normalize();
            Move_Pos(move_vel);
        }
    }

    public override void Reach_Base()
    {
        if (m_state == State.In_Base) return;

        m_state = State.In_Base;
        m_in_base = true;

        m_player_chara_manager.Back_Base_Player_Chara(this, out m_in_base_wait_pos);
    }

    protected override void Enter_In_Search()
    {
        if (m_state == State.Move_To_Base)
        {
            m_state = State.Attack;
        }
    }

    protected override void Empty_In_Search()
    {
        if(m_state == State.Attack)
        {
            m_state = State.Move_To_Base;
        }
    }

    public override void Dead_Other_Chara(Game_Chara chara)
    {
        Out_Search_Chara(chara);
        Out_Landing_Skill_Area(chara);
    }

    public void Thrown()
    {
        m_state = State.Thrown;
        m_in_base = false;

        m_in_landing_skill_chara_list.Clear();

        m_player_chara_manager.Out_Base_Player_Chara(this);

        m_audio_source.PlayOneShot(m_thrown_se);

    }

    public void To_Land()
    {
        if (m_in_search_chara_list.Count > 0) m_state = State.Attack;
        else m_state = State.Move_To_Base;

        switch (m_landing_skill_kind)
        {
            case Landing_Skill_Kind.Attack:
                Attack_Landing_Skill();

                break;
            case Landing_Skill_Kind.Heal:
                Heal_Landing_Skill();

                break;
            case Landing_Skill_Kind.Enhance:
                Enhance_Landing_Skill();

                break;
        }

        GameObject effect_obj = Instantiate(m_landing_effect_obj);
        Vector3 effect_pos = transform.position;
        effect_pos.y = effect_obj.transform.position.y;
        effect_obj.transform.position = effect_pos;
        effect_obj.GetComponent<Game_Wave_Effect>().Initialize(transform.Find("Landing_Skill_Area").GetComponent<SphereCollider>().radius);
    }

    public void In_Landing_Skill_Area(Game_Chara chara)
    {
        m_in_landing_skill_chara_list.Add(chara);
    }

    public void Out_Landing_Skill_Area(Game_Chara chara)
    {
        m_in_landing_skill_chara_list.Remove(chara);
    }

    private void Attack_Landing_Skill()
    {
        for (int i = 0; i < m_in_landing_skill_chara_list.Count; ++i)
        {
            Game_Chara current_chara = m_in_landing_skill_chara_list[i];
            if (current_chara.Is_Player) continue;

            current_chara.Damage((int)(m_atk * m_landing_skill_arg));
        }

        m_audio_source.PlayOneShot(m_landing_attack_se);
    }

    private void Heal_Landing_Skill()
    {
        for (int i = 0; i < m_in_landing_skill_chara_list.Count; ++i)
        {
            Game_Chara current_chara = m_in_landing_skill_chara_list[i];
            if (!current_chara.Is_Player) continue;

            current_chara.Heal((int)(m_landing_skill_arg));
        }

        m_audio_source.PlayOneShot(m_landing_heal_se);
    }

    private void Enhance_Landing_Skill()
    {
        for (int i = 0; i < m_in_landing_skill_chara_list.Count; ++i)
        {
            Game_Chara current_chara = m_in_landing_skill_chara_list[i];
            if (!current_chara.Is_Player) continue;

            current_chara.Enhance(m_landing_skill_arg);
        }

        m_audio_source.PlayOneShot(m_landing_enhance_se);
    }

    public bool In_Base { get { return m_in_base; } }
}
