﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_Result : MonoBehaviour
{
    private enum State
    {
        Play, 
        Win, 
        Lose
    }

    private AudioSource m_bgm_audio_source;

    private AudioSource m_audio_source;
    private AudioClip m_win_se;
    private AudioClip m_lose_se;

    [SerializeField] private Text m_result_text;

    [SerializeField] private Fade m_fade;

    private State m_state;

    private float m_result_show_timer = 7.0f;

    // Start is called before the first frame update
    void Start()
    {
        m_bgm_audio_source = GameObject.Find("BGM_Player").GetComponent<AudioSource>();

        m_audio_source = GetComponent<AudioSource>();
        Game_SE_Manager se_manager = GameObject.Find("SE_Manager").GetComponent<Game_SE_Manager>();
        m_win_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Win);
        m_lose_se = se_manager.Get_SE(Game_SE_Manager.SE_Kind.Lose);

        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_state == State.Win || m_state == State.Lose)
        {
            m_result_show_timer -= Time.deltaTime;

            if(m_result_show_timer <= 0.0f)
            {
                m_fade.Fade_Out( () => SceneManager.LoadScene( "Title" ) );
            }
        }
    }

    private void Initialize()
    {
        m_state = State.Play;
        m_result_text.gameObject.SetActive(false);
    }

    public void Win()
    {
        if (m_state != State.Play) return;

        m_state = State.Win;

        m_result_text.text = "WIN!";
        m_result_text.color = new Color(1.0f, 0.3f, 0.0f);
        m_result_text.gameObject.SetActive(true);

        m_audio_source.PlayOneShot(m_win_se);
        m_bgm_audio_source.Stop();
    }

    public void Lose()
    {
        if (m_state != State.Play) return;

        m_state = State.Lose;

        m_result_text.text = "LOSE";
        m_result_text.color = new Color(0.0f, 0.3f, 1.0f);
        m_result_text.gameObject.SetActive(true);

        m_audio_source.PlayOneShot(m_lose_se);
        m_bgm_audio_source.Stop();
    }
}
