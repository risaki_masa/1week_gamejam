﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Effect_Area : MonoBehaviour
{
    public enum Effect_Kind
    {
        Change_Spd, 
        Dot, 
    }

    [SerializeField] private Effect_Kind m_effect_kind;
    [SerializeField] private float m_effect_val;
    [SerializeField] private float m_effect_per_time;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Effect_Kind Kind { get { return m_effect_kind; } }
    public float Effect_Val { get { return m_effect_val; } }
    public float Effect_Per_Time { get { return m_effect_per_time; } }
}
