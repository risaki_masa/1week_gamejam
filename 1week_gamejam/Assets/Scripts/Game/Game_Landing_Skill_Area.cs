﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Landing_Skill_Area : MonoBehaviour
{
    private Game_Player_Chara m_player;

    // Start is called before the first frame update
    void Start()
    {
        m_player = transform.parent.GetComponent<Game_Player_Chara>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        Game_Chara enter_chara = other.GetComponent<Game_Chara>();
        if(enter_chara != null)
        {
            m_player.In_Landing_Skill_Area(enter_chara);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Game_Chara exit_chara = other.GetComponent<Game_Chara>();
        if(exit_chara != null)
        {
            m_player.Out_Landing_Skill_Area(exit_chara);
        }
    }
}
