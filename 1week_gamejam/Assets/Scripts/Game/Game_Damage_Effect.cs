﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Damage_Effect : MonoBehaviour
{
    [SerializeField] private float m_show_time;

    private float m_show_timer;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (m_show_timer > 0.0f)
        {
            m_show_timer -= Time.deltaTime;

            if (m_show_timer <= 0.0f)
            {
                m_show_timer = 0.0f;
                gameObject.SetActive(false);
            }
        }
    }

    public void Initialize()
    {
        m_show_timer = 0.0f;

        gameObject.SetActive(false);
    }

    public void Fire()
    {
        m_show_timer = m_show_time;

        gameObject.SetActive(true);
    }
}
